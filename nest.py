import docker
from time import sleep

import db
import shell
import timedkill

testtube_limit_seconds = 30
client = docker.from_env()
NEST = {}
IMG = "rubyshadows/heartbeat:v4"

def clear_containers():
    '''
        TEMP FUNC: clears all containers from memory 
    '''
    for container in client.containers.list(all=True):
        kill(container)


def heal_container(container):
    '''puts heart into container'''
    c_name = container.name

    filename = "heart"
    text = ""
    with open(filename, 'r') as fd:
        text = fd.read()
    row = 0
    col = 0

    file_obj = shell.create_file(filename, text, row, col)
    file_id = file_obj['fileid']
    file_name = file_obj['filename']
    file_type = file_obj['filetype']

    responding = check_container(container)
    if responding:
        shell.copy_file(c_name, file_id, file_name)
        has_heart = shell.extract_heart(c_name)
    else:
        has_heart = None

    return {"has_heart": has_heart}

def save_container(user):
    '''
        Commit and save container to dockerhub
    '''
    pass

def load_container(user):
    '''
        TODO: Pull container from dockerhub and return it
        If none on dockerhub, create new one
    '''
    if NEST.get(user.id) != None:
        return NEST.get(user.id);
    return new_container(user)


def new_container(user):
    '''
        Remove old container and create new one
    '''
    remove_container(user)
    NEST[user.id] = client.containers.run(IMG, detach=True) 
    user.container_name = NEST[user.id].name
    db.save()
    return NEST[user.id]

def user_container(user):
    '''
        Finds running container on machine and returns it
    '''
    return NEST.get(user.id)

def kill(container):
    '''
        Remove a container from memory
    '''
    try:
        container.remove(force=True)
    except Exception as e:
        pass

def place(container, file_obj):
    '''
        place file in container
    '''
    c_name = container.name
    file_id = file_obj['fileid']
    file_name = file_obj['filename']
    file_type = file_obj['filetype']
    copy_good = shell.copy_file(c_name, file_id, file_name)
    if copy_good:
        message = "Successfully placed"
    else:
        message = "Could not place {}".format(file_name)
    output = {"output": message, "error":False}
    responding = check_container(container)
    if responding:
        has_heart = shell.extract_heart(c_name)
    else:
        has_heart = None
        print("run: container not responding")
    material = 0
    ret = {"output": output.get('output'), "file_obj":file_obj, "has_heart": has_heart, "material": material}
    return ret

def place_file(user, file_obj):
    '''
        place file in container
    '''
    container = NEST.get(user.id)
    if container is None or not check_container(container):
        print("container not alive, resetting it")
        container = new_container(user)
    return place(container, file_obj)

def remove_container(user):
    '''
        Remove container from memory
    ''' 
    if NEST.get(user.id) == None:
        return
    else:
        container = NEST.get(user.id)
        # save_container(user)
        del NEST[user.id]
        kill(container)

def run(container, file_obj):
    '''
        Run file inside container, return output and hasHeart
    '''
    c_name = container.name
    file_id = file_obj['fileid']
    file_name = file_obj['filename']
    file_type = file_obj['filetype']

    copy_good = shell.copy_file(c_name, file_id, file_name)
    output = shell.execute_file(c_name, file_name, file_type)
    print("output: {}".format(output))
    responding = check_container(container)
    if responding:
        has_heart = shell.extract_heart(c_name)
    else:
        has_heart = None
        print("run: container not responding")

    material = 0

    if "python" in file_type:
        material = 6
    elif "bash" in file_type:
        material = 2
    else:
        material = 10

    if output.get('error') is False:
        if has_heart:
            pass
        else:
            material *= 10
    else:
        material = 0

    ret = {"output": output.get('output'), "file_obj":file_obj, "has_heart": has_heart, "material": material}
    print("returns: ")
    print(ret)
    return ret


def run_file(user, file_obj):
    '''
        Run a file within container, return output and hasHeart
    '''
    container = NEST.get(user.id)
    if container is None or not check_container(container):
        print("container not alive, resetting it")
        container = new_container(user)
    return run(container, file_obj)


def test_file(file_obj):
    '''
        Copy file into container, execute file in container, return output
    '''
    testtube = client.containers.run(IMG, detach=True) 
    timed_out = {"state": False}
    def destroy_testtube():
            '''
                Destroys testtube
            '''
            timedkill.kill_testtube(testtube, testtube_limit_seconds, timed_out)

    print("start cleaner for {} seconds.".format(testtube_limit_seconds))
    cleaner = timedkill.RepeatedTimer(testtube_limit_seconds, destroy_testtube)
    cleaner.start()
    result = run(testtube, file_obj)
    kill(testtube)
    cleaner.stop()
    if timed_out['state']:
        result['running'] = True
        result['material'] = 1
    print("timedout state: {}".format(timed_out['state']))
    return result

def check_container(container):
    """ Checks whether container is running """
    for c in client.containers.list():
        if c.id == container.id:
            container = c
            break

    if container.status == "running":
        return True
    else:
        return False