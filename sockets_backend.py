'''
	This module modifies:
		user.row
		user.col - maybe
'''

from flask import Flask, render_template, request
from flask_socketio import SocketIO, send, emit

SOCKET_NEST = {} # sid : user | none

def init_sockets(ap):
	app = ap
	app.config['SECRET_KEY'] = 'secret!'
	socketio = SocketIO(app)

	def get_client(sid):
		return SOCKET_NEST.get(sid)

	################### Connect and Disconnect ############################
	@socketio.on('connect')
	def connect():
		'''
			Character is created.
			ties sid with userid
		'''
		sid = request.sid
		print(sid)
		print('connect recieved message')

	@socketio.on('disconnect')
	def disconnect():
		'''
			Character is vanished
			sid is tied with userid
				could use disconnect sid to save and remove containers
		'''
		sid = request.sid
		user = get_client(sid)
		if user is None:
			print('disconnect recieved message from anonymous')
		else:
			print('disconnect recieved message from {}'.format(user))

		if SOCKET_NEST.get(sid):
			del SOCKET_NEST[sid]
		emit("departure", sid, broadcast=True)

	@socketio.on('fetch_everyone')
	def fetch_everyone(data):
		sid = request.sid
		people = []
		for k,v in SOCKET_NEST.items():
			people.append(v)
			print("adding person")
			print(v)
		return sid, people

	################### Broadcast Requests ############################
	@socketio.on('request_arrival')
	def request_arrival(userData):
		sid = request.sid
		userData['sid'] = sid
		SOCKET_NEST[sid] = userData
		emit('arrival', userData, broadcast=True)

	@socketio.on('request_drop')
	def request_drop(scriptData):
		sid = request.sid
		scriptData['sid'] = sid
		emit('drop', scriptData, broadcast=True)

	@socketio.on('request_ghost')
	def request_ghost():
		print('emit ghost')
		emit('ghost', request.sid, broadcast=True)

	@socketio.on('request_move')
	def request_move(coordinates):
		sid = request.sid
		row = coordinates['row']
		col = coordinates['col']
		if SOCKET_NEST.get(sid) is None:
			print('lockune movement repressed')
			return
		SOCKET_NEST[sid]['row'] = row
		SOCKET_NEST[sid]['col'] = col
		emit('movePlayer', {'sid': sid, 'row': row, 'col': col}, broadcast=True)

	@socketio.on('request_unghost')
	def request_unghost():
		print("emit unghost")
		emit('unghost', request.sid, broadcast=True)

	@socketio.on('request_update')
	def request_update(userData):
		sid = request.sid
		userData['sid'] = sid
		SOCKET_NEST[sid] = userData
		emit('update', userData, broadcast=True)

	socketio.run(app, host="0.0.0.0", port="5001")


if __name__ == '__main__':
	app = Flask(__name__)
	socketio = init_sockets(app)