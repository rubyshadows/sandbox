# Use an official Python runtime as a parent image
FROM python:3.7-slim

# Set the working directory to /home
WORKDIR /home

# Copy the current directory contents into the container at /home
COPY . /home

# Don't exclude man pages & other documentation
RUN rm /etc/dpkg/dpkg.cfg.d/docker

# Reinstall all currently installed packages to get man pages back
RUN apt-get update && \
    dpkg -l | grep ^ii | cut -d' ' -f3 | xargs apt-get install -y --reinstall && \
    rm -r /var/lib/apt/lists/* 

RUN apt-get update -y && apt-get install man -y

RUN apt-get install python3-pip -y 
RUN pip3 install requests
RUN apt-get install gcc -y
RUN apt-get install git -y && apt-get install curl -y && apt-get install wget -y
RUN apt-get install ruby-full -y
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install nodejs -y
# https://stackoverflow.com/questions/20392243/run-c-sharp-code-on-linux-terminal
# https://stackoverflow.com/questions/14788345/how-to-install-the-jdk-on-ubuntu-linux

# Run heart when the container launches
CMD ["python", "heart"]
