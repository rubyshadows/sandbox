import datetime
from threading import Timer
from time import sleep

def kill_testtube(testtube, seconds, timed_out):
    '''
        Destroys testing container after an interval of time
    '''
    print("kill invoked, timedout: {}".format(timed_out['state']))
    #sleep(seconds)
    try:
        print("killing")
        testtube.remove(force=True)
        print("kill success")
        timed_out["state"] = True
    except Exception as e:
        print("kill interrupted")
        pass


class RepeatedTimer(object):
    '''
        https://stackoverflow.com/questions/3393612/run-certain-code-every-n-seconds
        Object to scan inactive for containers every interval
    '''
    def __init__(self, interval, function, *args, **kwargs):
        self._timer     = None
        self.interval   = interval
        self.function   = function
        self.args       = args
        self.kwargs     = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False



# try:
    # run (testtube, file)
    # return run
# catch:
    #return {"result", "still running"}
